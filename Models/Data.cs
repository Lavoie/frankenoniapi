using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;



namespace Ex2API.Models
{
    public class Data
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }      

        public string SharePrice { get; set; }
        public string Volume { get; set; }
        public Decimal SharePriceChange { get; set; }
        public string DT_Moscow { get; set; }
        public DateTime DT_Mtl { get; set; }
        public string CurrentTempMtl { get; set; }
        public string SunCloudMtl { get; set; }
        public CgiNews[] LastNews { get; set; }
        public string Meaning { get; set; }
    }

}



