﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ex2API.Models;
using Ex2API.Services;
using Microsoft.AspNetCore.Mvc;

namespace Ex2API.Controllers {
    [Route ("api/[controller]")]
    [ApiController]
    public class DataController : ControllerBase {
        private readonly DataService _dataService;


        public DataController (DataService dataservice) {
            _dataService = dataservice;
        }

        [HttpGet]
        public ActionResult<List<Data>> Get () {
            
            
            return Ok(_dataService.Get ());
        }

        [HttpGet ("id/{id:length(24)}", Name = "GetById")]
        public ActionResult<Data> GetID (string id) {
          var data =  _dataService.GetID (id);

            if (data == null) {
                return NotFound ();
            }


             return Ok(data);
        }
        //* Format date YYYYMMDD
        [HttpGet ("date/{date:length(8)}", Name = "GetByDate")]
        public ActionResult<Data> GetDate (int date) {
            Data data = _dataService.GetDate (date);

            if (data == null) {
                return NotFound ();
            }
        
            
            return data;
        }

    }
}