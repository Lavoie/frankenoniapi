
namespace Ex2API.Services

{
    public class TimerStart
    {
        public static System.Timers.Timer aTimer;      


         public static void SetTimer()
         {
             // Create a timer with a two second interval.
             aTimer = new System.Timers.Timer(30000);
             // Hook up the Elapsed event for the timer. 
             aTimer.Elapsed += TimeLoop.OnTimedEvent;
             aTimer.AutoReset = true;
             aTimer.Enabled = true;
         }

        
    }
}