using Ex2API.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ex2API.Services
{

    public class StockService
    {
        public string key = "Z188G9ES0UTGOJP5";
        public Decimal today_stock_value;
        public Decimal yesterday_stock_value;
        public decimal stockDiff;

        public async Task<String> GetStockValue()
        {
            string Url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=GIB-A.To&outputsize=compact&apikey=" + key;
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JEnumerable<JToken> Stockdata = JObject.Parse(data).Children();

                    List<JToken> tokens = Stockdata.Children().ToList();


                    string _Stockdata = tokens[1].Children().ToList()[0].Children().ToList()[0].Value<string>("1. open");

                    //% stock value change************************************************************************************************
                    string _StockdataYesterday = tokens[1].Children().ToList()[1].Children().ToList()[0].Value<string>("4. close");

                    today_stock_value = decimal.Parse(_Stockdata);
                    yesterday_stock_value = decimal.Parse(_StockdataYesterday);
                    stockDiff = (((yesterday_stock_value - today_stock_value) / Math.Abs((yesterday_stock_value + today_stock_value) / 2)) * 100);

                    //********************************************************************************************************************

                    return _Stockdata;
                }
            }
            return null;
        }

        public async Task<String> GetStockVolume()
        {

            string Url = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=GIB-A.To&outputsize=compact&apikey=" + key;
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JObject Volumedata = JObject.Parse(data);

                    //List<JToken> tokens = Volumedata.Children().ToList();


                    string _Volumedata = Volumedata["Time Series (Daily)"].Children().ToList()[0].Children().ToList()[0].Value<string>("5. volume");

                    return _Volumedata;
                }
            }
            return null;
        }
    }

}