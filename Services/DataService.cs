using System;
using System.Collections.Generic;
using System.Linq;
using Ex2API.Models;
using MongoDB.Driver;

namespace Ex2API.Services {
    public class DataService {
        private readonly IMongoCollection<Data> _datas;

        public DataService (IDatabaseSettings settings) {

            MongoClient client = new MongoClient ("mongodb://172.17.0.3:27017");
            IMongoDatabase database = client.GetDatabase ("admin");

            _datas = database.GetCollection<Data> ("stockMarketDb");
        }

        public List<Data> Get () =>
            _datas.Find (data => true).ToList ();

        // * format date  YYYYMMDD  
        public Data GetDate (int date) {
            int[] splitDate = new int[3];

            //Year
            splitDate[0] = int.Parse (date.ToString ().Substring (0, 4));

            //Month
            splitDate[1] = int.Parse (date.ToString ().Substring (4, 2));

            //Day
            splitDate[2] = int.Parse (date.ToString ().Substring (6, 2));


            DateTime tempDate = new DateTime(splitDate[0],splitDate[1],splitDate[2]);


            IEnumerable<Data> data = from _stock in _datas.Find(t => true).ToEnumerable()
                where _stock.DT_Mtl.Year == splitDate[0] &&
                    _stock.DT_Mtl.Month == splitDate[1] &&
                    _stock.DT_Mtl.Day == splitDate[2]
                select _stock;
                
            return data.FirstOrDefault ();
        }
        public Data GetID (string id) =>
            _datas.Find<Data> (data => data.Id == id).FirstOrDefault ();

        public Data Create (Data data) {
            _datas.InsertOne (data);
            return data;
        }

        public void Update (string id, Data dataIn) =>
            _datas.ReplaceOne (data => data.Id == id, dataIn);

        public void Remove (Data dataIn) =>
            _datas.DeleteOne (data => data.Id == dataIn.Id);

        public void Remove (string id) =>
            _datas.DeleteOne (data => data.Id == id);
    }
}