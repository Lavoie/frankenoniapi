using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ex2API.Services
{

    public class NumberMEaning
    {

        public async Task<String> GetNumberMeaning()
        {

            string Url = "http://numbersapi.com/random/year?json";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JObject meaning = JObject.Parse(data);
                    string _meaningdata = (string)meaning["text"];

                    return _meaningdata;
                }
            }
            return null;
        }

    }


}


    
