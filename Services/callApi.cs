using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System;
using Ex2API.Models;
namespace Ex2API.Services
{
    public class callApi
    {
        public static async Task<string> get(string baseUrl)
        {
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(baseUrl))
            using (HttpContent content = res.Content)
            {
                return await content.ReadAsStringAsync();
            }
        }

         public static async Task<string> post(string baseUrl,Data dt)
        {
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.PostAsJsonAsync(baseUrl,JsonConvert.SerializeObject(dt)))
            using (HttpContent content = res.Content)
            {
                return await content.ReadAsStringAsync();
            }
        }
    }
}