using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System;

namespace Ex2API.Services
{
    public class DateTimeService
    {
        public async Task<string> GetMoscowTime()
        {
            string Url_Mcw = "http://api.geonames.org/timezoneJSON?lat=55.7558&lng=37.6173&username=cddl1445";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url_Mcw))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
              
                {

                    dynamic MoscowlTime = JValue.Parse(data);

                    string Moscowdt = MoscowlTime.time;

                    return Moscowdt;
                }
            }
           
        }
        public async Task<DateTime> GetMontrealTime()
        {

            string Url_Mtl = "http://api.geonames.org/timezoneJSON?lat=45.5017&lng=-73.5673&username=cddl1445";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url_Mtl))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
              
                {

                    dynamic MtlTime = JValue.Parse(data);

                    DateTime Mdt = MtlTime.time;

                    return Mdt;
                }            
            }
        }

    }
}