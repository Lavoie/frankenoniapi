using System;
using System.Timers;
using Ex2API.Models;
using Ex2API.Services;

namespace Ex2API.Services
{

    public class TimeLoop
    {
        public static async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Data dt = new Data();
            DateTimeService datetime = new DateTimeService();
            WeatherService weather = new WeatherService();
            StockService stock_value = new StockService();
            NumberMEaning meaning = new NumberMEaning();
            NewsService Cgi_news = new NewsService();



            dt.DT_Moscow = await datetime.GetMoscowTime();
            dt.DT_Mtl = await datetime.GetMontrealTime();
            dt.SunCloudMtl = await weather.GetWeather();
            dt.CurrentTempMtl = await weather.GetTemp() + "°C";
            dt.SharePrice = await stock_value.GetStockValue() + "$";
            //dt.Volume = await stock_value.GetStockVolume();
            dt.Meaning = await meaning.GetNumberMeaning();
            dt.SharePriceChange = stock_value.stockDiff;
            dt.LastNews = await Cgi_news.GetCgiNews();

            //! important
            await callApi.post("https://localhost:2020/api/Data",dt);


            if (Convert.ToInt32(dt.Volume) >= 300000)
            {
                if (dt.DT_Mtl.DayOfWeek != DayOfWeek.Sunday && dt.DT_Mtl.DayOfWeek != DayOfWeek.Saturday)
                {
                    if (dt.DT_Mtl.Hour > 9 && dt.DT_Mtl.Hour < 17)
                    {
                        
                    }
                }
            }
        }
    }
}