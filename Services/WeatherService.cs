using Ex2API.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace Ex2API.Services
{
    public class WeatherService
    {
        public string key = "185b269f0aa53691b9a99365242ef581";
        public async Task<String> GetWeather()
        {


            string Url = "https://api.openweathermap.org/data/2.5/weather?id=6077243&appid=" + key + "&units=metric";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JEnumerable<JToken> Wdata = JObject.Parse(data).Children();

                    List<JToken> tokens = Wdata.Children().ToList();
           
                    string _weather = tokens[1].Children().ToList()[0].Value<string>("main");

                    return _weather;
                }
            }
            return null;
        }
        public async Task<String> GetTemp()
        {

            string Url = "https://api.openweathermap.org/data/2.5/weather?id=6077243&appid=" + key + "&units=metric";
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {
                    JObject temp = JObject.Parse(data);
                    string _temp = (string)temp["main"]["temp"];

                    return _temp;
                }
            }
            return null;
        }
    }
}