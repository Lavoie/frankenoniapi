using Ex2API.Models;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;


namespace Ex2API.Services
{

    public class NewsService
    {
        public async Task<CgiNews[]> GetCgiNews()
        {

            string Url = "https://feed2json.org/convert?url=https%3A%2F%2Ffeeds.finance.yahoo.com%2Frss%2F2.0%2Fheadline%3Fs%3DGIB-A.To%26region%3DUS%26lang%3Den-US";
            CgiNews[] _cginews = new CgiNews[5];
            using (HttpClient client = new HttpClient())

            using (HttpResponseMessage res = await client.GetAsync(Url))
            using (HttpContent content = res.Content)
            {
                string data = await content.ReadAsStringAsync();
                if (data != null)
                {


                    JEnumerable<JToken> Newsdata = JObject.Parse(data).Children();

                    List<JToken> tokens = Newsdata.Children().ToList();



                    for (int i = 0; i < _cginews.Count(); i++)
                    {
                        CgiNews Tempnews = new CgiNews();

                        Tempnews.date_published = tokens[3].Children().ToList()[i].Value<string>("date_published");
                        Tempnews.url = tokens[3].Children().ToList()[i].Value<string>("url");                       
                        Tempnews.summary = tokens[3].Children().ToList()[i].Value<string>("summary");
                        Tempnews.title = tokens[3].Children().ToList()[i].Value<string>("title");

                        _cginews[i] = Tempnews;


                    }

                    return _cginews;
                }
            }
            return null;
        }

    }



























}